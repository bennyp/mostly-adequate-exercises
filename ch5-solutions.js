import add from 'ramda/es/add'
import compose from 'ramda/es/compose'
import concat from 'ramda/es/concat'
import filter from 'ramda/es/filter'
import flip from 'ramda/es/flip'
import head from 'ramda/es/head'
import last from 'ramda/es/last'
import map from 'ramda/es/map'
import prop from 'ramda/es/prop'
import reduce from 'ramda/es/reduce'
import replace from 'ramda/es/replace'
import sortBy from 'ramda/es/sortBy'
import toLower from 'ramda/es/toLower'

import {formatMoney} from './lib/accounting'

// Example Data
const CARS = [{
  name: 'Ferrari FF',
  horsepower: 660,
  dollar_value: 700000,
  in_stock: true,
}, {
  name: 'Spyker C12 Zagato',
  horsepower: 650,
  dollar_value: 648000,
  in_stock: false,
}, {
  name: 'Jaguar XKR-S',
  horsepower: 550,
  dollar_value: 132000,
  in_stock: false,
}, {
  name: 'Audi R8',
  horsepower: 525,
  dollar_value: 114200,
  in_stock: false,
}, {
  name: 'Aston Martin One-77',
  horsepower: 750,
  dollar_value: 1850000,
  in_stock: true,
}, {
  name: 'Pagani Huayra',
  horsepower: 700,
  dollar_value: 1300000,
  in_stock: false,
}];

// Exercise 1:
// ============
// Use _.compose() to rewrite the function below. Hint: _.prop() is curried.

const isLastInStock = compose(prop('in_stock'), last)

console.log(isLastInStock(CARS))

// Exercise 2:
// ============
// Use _.compose(), _.prop() and _.head() to retrieve the name of the first car.
// const nameOfFirstCar = undefined;
const nameOfFirstCar = compose(prop('name'), head);

console.log(nameOfFirstCar(CARS))


// Exercise 3:
// ============
// Use the helper function _average to refactor averageDollarValue as a composition.
const _average = xs => reduce(add, 0, xs) / xs.length; // <- leave be

const averageDollarValue = compose(_average, map(prop('dollar_value')))

console.log(composedaverageDollarValue(CARS))

// Exercise 4:
// ============
// Write a function: sanitizeNames() using compose that returns a list of
// lowercase and underscored car's names: e.g:
// sanitizeNames([{name: 'Ferrari FF', horsepower: 660, dollar_value: 700000, in_stock: true}]) //=> ['ferrari_ff'].

const _underscore = replace(/\W+/g, '_'); //<-- leave this alone and use to sanitize

const sanitizeNames = map(compose(_underscore, toLower, prop('name')));

console.log(sanitizeNames(CARS))

// Bonus 1:
// ============
// Refactor availablePrices with compose.

const trace = tag => x => {
  console.log(tag, x);
  return x;
};

const join = str => arr => arr.join(str);
const inStock = filter(x => x.in_stock);
const formattedDollarValue = x => formatMoney(x.dollar_value);
const commaSep = join(', ');

const availablePrices = compose(commaSep, map(formattedDollarValue), inStock);

console.log(inStock(CARS))
console.log(availablePrices(CARS));


// Bonus 2:
// ============
// Refactor to pointfree. Hint: you can use _.flip().

const fastestCar = compose(
  x => x + ' is the fastest',
  trace('after name'),
  prop('name'),
  last,
  sortBy(prop('horsepower'))
)

console.log(fastestCar(CARS))
