import add from 'ramda/es/add'
import compose from 'ramda/es/compose'
import concat from 'ramda/es/concat'
import filter from 'ramda/es/filter'
import flip from 'ramda/es/flip'
import head from 'ramda/es/head'
import last from 'ramda/es/last'
import map from 'ramda/es/map'
import prop from 'ramda/es/prop'
import reduce from 'ramda/es/reduce'
import replace from 'ramda/es/replace'
import sortBy from 'ramda/es/sortBy'
import toLower from 'ramda/es/toLower'

import {formatMoney} from './lib/accounting'

// Example Data
const CARS = [{
  name: 'Ferrari FF',
  horsepower: 660,
  dollar_value: 700000,
  in_stock: true,
}, {
  name: 'Spyker C12 Zagato',
  horsepower: 650,
  dollar_value: 648000,
  in_stock: false,
}, {
  name: 'Jaguar XKR-S',
  horsepower: 550,
  dollar_value: 132000,
  in_stock: false,
}, {
  name: 'Audi R8',
  horsepower: 525,
  dollar_value: 114200,
  in_stock: false,
}, {
  name: 'Aston Martin One-77',
  horsepower: 750,
  dollar_value: 1850000,
  in_stock: true,
}, {
  name: 'Pagani Huayra',
  horsepower: 700,
  dollar_value: 1300000,
  in_stock: false,
}];

// Exercise 1:
// ============
// Use _.compose() to rewrite the function below. Hint: _.prop() is curried.

const isLastInStock = cars => {
  const last_car = last(cars);
  return prop('in_stock', last_car);
};

console.log(isLastInStock(CARS))


// Exercise 2:
// ============
// Use _.compose(), _.prop() and _.head() to retrieve the name of the first car.
const nameOfFirstCar = undefined;


// Exercise 3:
// ============
// Use the helper function _average to refactor averageDollarValue as a composition.
const _average = xs => reduce(add, 0, xs) / xs.length; // <- leave be

const averageDollarValue = cars => {
  const dollar_values = map(c => c.dollar_value, cars);
  return _average(dollar_values);
};

console.log(averageDollarValue(CARS))

// Exercise 4:
// ============
// Write a function: sanitizeNames() using compose that returns a list of
// lowercase and underscored car's names: e.g:
// sanitizeNames([{name: 'Ferrari FF', horsepower: 660, dollar_value: 700000, in_stock: true}]) //=> ['ferrari_ff'].

const _underscore = replace(/\W+/g, '_'); //<-- leave this alone and use to sanitize

const sanitizeNames = undefined;

// Bonus 1:
// ============
// Refactor availablePrices with compose.

const availablePrices = cars => {
  const available_cars = filter(prop('in_stock'), cars);
  return available_cars
    .map(x => accounting.formatMoney(x.dollar_value))
    .join(', ');
};

console.log(availablePrices(CARS));


// Bonus 2:
// ============
// Refactor to pointfree. Hint: you can use _.flip().

const fastestCar = cars => {
  const sorted = sortBy(car => car.horsepower, cars);
  const fastest = last(sorted);
  return fastest.name + ' is the fastest';
};

console.log(fastestCar(CARS))
